/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.variables.stamper;

import static com.marvelution.bamboo.plugins.variables.stamper.configuration.BuildVariablesStamperConfigurator.CFG_FILENAME;
import static com.marvelution.bamboo.plugins.variables.stamper.configuration.BuildVariablesStamperConfigurator.DEFAULT_FILENAME;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.build.logger.SimpleBuildLogger;
import com.atlassian.bamboo.configuration.ConfigurationMapImpl;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskState;
import com.atlassian.bamboo.util.BuildUtils;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.agent.ExecutableBuildAgent;
import com.atlassian.bamboo.v2.build.agent.capability.AgentContext;
import com.atlassian.bamboo.variable.CustomVariableContext;
import com.google.common.collect.Maps;
import com.google.common.io.Files;

/**
 * Testcase for the {@link BuildVariablesStamper}
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 2.0.0
 */
@RunWith(MockitoJUnitRunner.class)
public class BuildVariablesStamperTest {

	private static final String BUILD_KEY = "BVS-DEVELOP-JOB1";
	private static final String BUILD_NUMBER = "1";
	private static final String BUILD_PLAN_NAME = "develop";
	private static final String AGENT_NAME = "AGENT-007";

	private static final String KEY_AGENT_NAME = "bamboo.agent";
	private static final String KEY_BUILD_KEY = "buildKey";
	private static final String KEY_BAMBOO_BUILD_NUMBER = "bamboo.buildNumber";
	private static final String KEY_BAMBOO_VERSION = "bamboo.version";
	private static final String KEY_BUILD_PLAN_NAME = "buildPlanName";
	private static final String KEY_BUILD_NUMBER = "buildNumber";
	private static final String KEY_BUILD_WORKING_DIRECTORY = "build.working.directory";

	private BuildVariablesStamper stamper;
	@Mock
	private AgentContext agentContext;
	@Mock
	private ExecutableBuildAgent buildAgent;
	@Mock
	private CustomVariableContext variableContext;
	@Mock
	private TaskContext taskContext;
	@Mock
	private BuildContext buildContext;
	private BuildLogger buildLogger;
	private File workingDirectory;
	private static Map<String, String> configuration;
	private static Map<String, String> variables;

	@BeforeClass
	public static void beforeClass() {
		configuration = Maps.newHashMap();
		configuration.put(CFG_FILENAME, DEFAULT_FILENAME);
		variables = Maps.newHashMap();
		variables.put(KEY_BAMBOO_BUILD_NUMBER, BuildUtils.getCurrentBuildNumber());
		variables.put(KEY_BAMBOO_VERSION, BuildUtils.getCurrentVersion());
		variables.put(KEY_BUILD_KEY, BUILD_KEY);
		variables.put(KEY_BUILD_NUMBER, BUILD_NUMBER);
		variables.put(KEY_BUILD_PLAN_NAME, BUILD_PLAN_NAME);
	}

	@Before
	public void before() throws Exception {
		workingDirectory = Files.createTempDir();
		variables.put(KEY_BUILD_WORKING_DIRECTORY, workingDirectory.getAbsolutePath());
		when(buildAgent.getName()).thenReturn(AGENT_NAME);
		when(taskContext.getConfigurationMap()).thenReturn(new ConfigurationMapImpl(configuration));
		when(taskContext.getBuildContext()).thenReturn(buildContext);
		when(taskContext.getWorkingDirectory()).thenReturn(workingDirectory);
		buildLogger = new SimpleBuildLogger();
		when(taskContext.getBuildLogger()).thenReturn(buildLogger);
		when(variableContext.getVariables(buildContext)).thenReturn(variables);
		stamper = new BuildVariablesStamper(agentContext, variableContext);
	}

	/**
	 * Test {@link BuildVariablesStamper#execute(TaskContext)} with an {@link ExecutableBuildAgent} set in the
	 * {@link AgentContext}
	 */
	@Test
	public void testExecuteWithBuildAgent() {
		when(agentContext.getBuildAgent()).thenReturn(buildAgent);
		try {
			assertThat(stamper.execute(taskContext).getTaskState(), is(TaskState.SUCCESS));
			Properties variables = getVariables();
			assertThat(variables.containsKey(KEY_AGENT_NAME), is(true));
			assertThat(variables.getProperty(KEY_AGENT_NAME), is(AGENT_NAME));
			validateCommonVariabels(variables);
		} catch (TaskException e) {
			fail("Stamper task threw an unexcepted exception: " + e.getMessage());
		}
	}

	/**
	 * Test {@link BuildVariablesStamper#execute(TaskContext)} with out an {@link ExecutableBuildAgent} within the
	 * {@link AgentContext}
	 */
	@Test
	public void testExecuteWithoutBuildAgent() {
		try {
			assertThat(stamper.execute(taskContext).getTaskState(), is(TaskState.SUCCESS));
			Properties variables = getVariables();
			assertThat(variables.containsKey(KEY_AGENT_NAME), is(false));
			validateCommonVariabels(variables);
		} catch (TaskException e) {
			fail("Stamper task threw an unexcepted exception: " + e.getMessage());
		}
	}

	/**
	 * Getter for the Variables as a {@link Properties} object. It will read from disk what the stamper wrote to disk
	 * 
	 * @return the {@link Properties}
	 */
	private Properties getVariables() {
		File file = new File(workingDirectory, DEFAULT_FILENAME);
		assertThat(file.exists(), is(true));
		Properties variables = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream(file);
			variables.load(input);
		} catch (Exception e) {
			fail("Failed to load written variables file '" + DEFAULT_FILENAME + "': " + e.getMessage());
		} finally {
			IOUtils.closeQuietly(input);
		}
		return variables;
	}

	/**
	 * Validate the common variables
	 * 
	 * @param variables the {@link Properties} object to validate
	 */
	private void validateCommonVariabels(Properties variables) {
		assertThat(variables.getProperty(KEY_BAMBOO_BUILD_NUMBER), is(BuildUtils.getCurrentBuildNumber()));
		assertThat(variables.getProperty(KEY_BAMBOO_VERSION), is(BuildUtils.getCurrentVersion()));
		assertThat(variables.getProperty(KEY_BUILD_KEY), is(BUILD_KEY));
		assertThat(variables.getProperty(KEY_BUILD_NUMBER), is(BUILD_NUMBER));
		assertThat(variables.getProperty(KEY_BUILD_PLAN_NAME), is(BUILD_PLAN_NAME));
		assertThat(buildLogger.getBuildLog().size(), is(1));
		assertThat(buildLogger.getErrorLog().size(), is(0));
	}

	/**
	 * Cleanup after the test
	 * 
	 * @throws Exception in case of errors
	 */
	@After
	public void after() throws Exception {
		workingDirectory.delete();
	}

}
