/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.variables.stamper.configuration;

import static com.marvelution.bamboo.plugins.variables.stamper.configuration.BuildVariablesStamperConfigurator.CFG_FILENAME;
import static com.marvelution.bamboo.plugins.variables.stamper.configuration.BuildVariablesStamperConfigurator.DEFAULT_FILENAME;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.mockito.Matchers.anyString;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.TaskConfiguratorHelperImpl;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.user.BambooAuthenticationContext;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.utils.error.SimpleErrorCollection;
import com.atlassian.bamboo.utils.i18n.I18nBean;
import com.google.common.collect.Maps;
import com.opensymphony.xwork.TextProvider;

/**
 * Testcase for the {@link BuildVariablesStamperConfigurator}
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 2.0.0
 */
@RunWith(MockitoJUnitRunner.class)
public class BuildVariablesStamperConfiguratorTest {

	private BuildVariablesStamperConfigurator configurator;
	@Mock
	private TextProvider textProvider;
	@Mock
	private I18nBean i18nBean;
	@Mock
	private BambooAuthenticationContext authenticationContext;
	@Mock
	private TaskDefinition taskDefinition;
	@Mock
	private ActionParametersMap params;

	/**
	 * Test Setup
	 */
	@Before
	public void before() {
		configurator = new BuildVariablesStamperConfigurator();
		configurator.setTaskConfiguratorHelper(new TaskConfiguratorHelperImpl(textProvider));
		when(i18nBean.getText(anyString())).then(new Answer<String>() {
			@Override
			public String answer(InvocationOnMock invocation) throws Throwable {
				return (String) invocation.getArguments()[0];
			}
		});
		when(authenticationContext.getI18NBean()).thenReturn(i18nBean);
		configurator.setAuthenticationContext(authenticationContext);
	}

	/**
	 * Test {@link BuildVariablesStamperConfigurator#generateTaskConfigMap(ActionParametersMap, TaskDefinition)}
	 */
	@Test
	public void testGenerateTaskConfigMap() {
		when(params.getString(CFG_FILENAME)).thenReturn(DEFAULT_FILENAME);
		Map<String, String> config = configurator.generateTaskConfigMap(params, taskDefinition);
		assertThat(config.containsKey(CFG_FILENAME), is(true));
		assertThat(config.get(CFG_FILENAME), is(DEFAULT_FILENAME));
	}

	/**
	 * Test {@link BuildVariablesStamperConfigurator#populateContextForCreate(Map)}
	 */
	@Test
	public void testPopulateContextForCreate() {
		Map<String, Object> context = Maps.newHashMap();
		configurator.populateContextForCreate(context);
		assertThat(context.containsKey(CFG_FILENAME), is(true));
		assertThat((String) context.get(CFG_FILENAME), is(DEFAULT_FILENAME));
	}

	/**
	 * Test {@link BuildVariablesStamperConfigurator#populateContextForEdit(Map, TaskDefinition)}
	 */
	@Test
	public void testPopulateContextForEdit() {
		Map<String, String> config = Maps.newHashMap();
		config.put(CFG_FILENAME, DEFAULT_FILENAME);
		when(taskDefinition.getConfiguration()).thenReturn(config);
		Map<String, Object> context = Maps.newHashMap();
		configurator.populateContextForEdit(context, taskDefinition);
		assertThat(context.containsKey(CFG_FILENAME), is(true));
		assertThat((String) context.get(CFG_FILENAME), is(DEFAULT_FILENAME));
	}

	/**
	 * Test {@link BuildVariablesStamperConfigurator#populateContextForView(Map, TaskDefinition)}
	 */
	@Test
	public void testPopulateContextForView() {
		Map<String, String> config = Maps.newHashMap();
		config.put(CFG_FILENAME, DEFAULT_FILENAME);
		when(taskDefinition.getConfiguration()).thenReturn(config);
		Map<String, Object> context = Maps.newHashMap();
		configurator.populateContextForView(context, taskDefinition);
		assertThat(context.containsKey(CFG_FILENAME), is(true));
		assertThat((String) context.get(CFG_FILENAME), is(DEFAULT_FILENAME));
	}

	/**
	 * Test {@link BuildVariablesStamperConfigurator#validate(ActionParametersMap, ErrorCollection)}
	 */
	@Test
	public void testValidateNoFileName() {
		ErrorCollection errorCollection = new SimpleErrorCollection();
		when(params.containsKey(CFG_FILENAME)).thenReturn(false);
		configurator.validate(params, errorCollection);
		assertThat(errorCollection.hasAnyErrors(), is(true));
		assertThat(errorCollection.getFieldErrors().get(CFG_FILENAME), is("build.variables.stamper.bvsFilename.mandatory"));
	}

	/**
	 * Test {@link BuildVariablesStamperConfigurator#validate(ActionParametersMap, ErrorCollection)}
	 */
	@Test
	public void testValidateEmptyFileName() {
		ErrorCollection errorCollection = new SimpleErrorCollection();
		when(params.containsKey(CFG_FILENAME)).thenReturn(true);
		when(params.getString(CFG_FILENAME)).thenReturn("");
		configurator.validate(params, errorCollection);
		assertThat(errorCollection.hasAnyErrors(), is(true));
		assertThat(errorCollection.getFieldErrors().get(CFG_FILENAME), is("build.variables.stamper.bvsFilename.mandatory"));
	}

	/**
	 * Test {@link BuildVariablesStamperConfigurator#validate(ActionParametersMap, ErrorCollection)}
	 */
	@Test
	public void testValidate() {
		ErrorCollection errorCollection = new SimpleErrorCollection();
		when(params.containsKey(CFG_FILENAME)).thenReturn(true);
		when(params.getString(CFG_FILENAME)).thenReturn(DEFAULT_FILENAME);
		configurator.validate(params, errorCollection);
		assertThat(errorCollection.hasAnyErrors(), is(false));
	}

}
