/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.variables.stamper;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Properties;

import org.apache.commons.io.IOUtils;

import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import com.atlassian.bamboo.util.BuildUtils;
import com.atlassian.bamboo.v2.build.agent.capability.AgentContext;
import com.atlassian.bamboo.variable.CustomVariableContext;
import com.marvelution.bamboo.plugins.variables.stamper.configuration.BuildVariablesStamperConfigurator;

/**
 * Simple {@link TaskType} implementation to store all the variables in a properties file
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class BuildVariablesStamper implements TaskType {

	private static final String CREATED_BY_COMMENT = "Created by the Marvelution Bamboo Variables Stamper plugin";

	private AgentContext agentContext;
	private CustomVariableContext variableContext;

	/**
	 * Constructor
	 *
	 * @param agentContext the {@link AgentContext}
	 * @param variableContext the {@link CustomVariableContext}
	 */
	public BuildVariablesStamper(AgentContext agentContext, CustomVariableContext variableContext) {
		this.agentContext = agentContext;
		this.variableContext = variableContext;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TaskResult execute(TaskContext taskContext) throws TaskException {
		final String filename = taskContext.getConfigurationMap().get(BuildVariablesStamperConfigurator.CFG_FILENAME);
		Properties variables = new Properties();
		variables.putAll(variableContext.getVariables(taskContext.getBuildContext()));
		// Add agent context variables
		if (agentContext != null && agentContext.getBuildAgent() != null) {
			variables.setProperty("bamboo.agent", agentContext.getBuildAgent().getName());
		}
		variables.setProperty("bamboo.version", BuildUtils.getCurrentVersion());
		variables.setProperty("bamboo.buildNumber", BuildUtils.getCurrentBuildNumber());
		FileOutputStream outputStream = null;
		try {
			// Add any extra variables that are not in the variable context
			outputStream = new FileOutputStream(new File(taskContext.getWorkingDirectory(), filename));
			variables.store(outputStream, CREATED_BY_COMMENT);
			taskContext.getBuildLogger().addBuildLogEntry("Stored the Build variables in " + filename);
		} catch (Exception e) {
			taskContext.getBuildLogger().addErrorLogEntry("Failed to store the Build variables in " + filename, e);
		} finally {
			IOUtils.closeQuietly(outputStream);
		}
		return TaskResultBuilder.create(taskContext).success().build();
	}

}
