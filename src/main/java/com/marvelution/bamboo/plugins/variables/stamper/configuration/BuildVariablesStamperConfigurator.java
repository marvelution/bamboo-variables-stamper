/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.bamboo.plugins.variables.stamper.configuration;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.google.common.collect.ImmutableList;
import com.marvelution.bamboo.plugins.variables.stamper.BuildVariablesStamper;

/**
 * {@link BuildVariablesStamper} configurator
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 2.0.0
 */
public class BuildVariablesStamperConfigurator extends AbstractTaskConfigurator {

	public static final String CFG_FILENAME = "bvsFilename";
	public static final String DEFAULT_FILENAME = "build-variables.properties";

	private static final List<String> FIELDS_TO_COPY = ImmutableList.of(CFG_FILENAME);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, String> generateTaskConfigMap(ActionParametersMap params, TaskDefinition previousTaskDefinition) {
		Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
		taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(config, params, FIELDS_TO_COPY);
		return config;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void populateContextForCreate(Map<String, Object> context) {
		super.populateContextForCreate(context);
		context.put(CFG_FILENAME, DEFAULT_FILENAME);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void populateContextForEdit(Map<String, Object> context, TaskDefinition taskDefinition) {
		super.populateContextForEdit(context, taskDefinition);
		taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void populateContextForView(Map<String, Object> context, TaskDefinition taskDefinition) {
		super.populateContextForView(context, taskDefinition);
		taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void validate(ActionParametersMap params, ErrorCollection errorCollection) {
		super.validate(params, errorCollection);
		if (!params.containsKey(CFG_FILENAME) || (params.containsKey(CFG_FILENAME) && StringUtils.isEmpty(params.getString(CFG_FILENAME)))) {
			errorCollection.addError(CFG_FILENAME, getI18nBean().getText("build.variables.stamper.bvsFilename.mandatory"));
		}
	}

}
