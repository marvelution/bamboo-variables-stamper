This project features a Bamboo plugin for making the build properties available in a properties file within the build directory
More details on <https://marvelution.atlassian.net/wiki/display/BAMVARSTA>

Issue Tracker
=============
<https://marvelution.atlassian.net/browse/BAMVARSTA>

License
=======
[The Apache Software License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)
